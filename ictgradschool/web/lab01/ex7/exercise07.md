###Herbivorous
* Capybara
* Horse
* Cow
* Chicken
* Sheep


###Carnivorous
* Frog
* Rat
* Fox
* Raccoon

###Omnivorous
* Cat
* Dog
* Hawk
* Polar Bear
* Lion